#!/bin/sh
CP="lib/*;classes"
SP=src/java/
APPLICATION="DIRA"

/bin/rm -f ${APPLICATION}.jar
/bin/rm -f ${APPLICATION}service.jar
/bin/rm -rf classes
/bin/mkdir -p classes/
/bin/rm -rf addons/classes
/bin/mkdir -p addons/classes/

javac -verbose -encoding utf8 -sourcepath "${SP}" -classpath "${CP}" -d classes/ src/java/nxt/*.java src/java/nxt/*/*.java src/java/nxt/*/*/*.java src/java/nxtdesktop/*.java 


echo "nxt class files compiled successfully"
read -n 1 -p "Press any key to continue"

ls addons/src/*.java > /dev/null 2>&1
read -n 1 -p "Press any key to continue"
javac -encoding utf8 -sourcepath "${SP}" -classpath "${CP}" -d addons/classes addons/src/*.java 

echo "addon class files compiled successfully"
read -n 1 -p "Press any key to continue"
