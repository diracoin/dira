# Welcome to DIRA #

## What is DIRA? ##
DIRA is a new cryptocurrency based on the NXT platform.

- Fast, secure, anonymous, cheap

- Proof of Stake and Central rewarding system (CRS) with Proof of Work: gain DIRA by staking your own tokens with a pure Proof-of-Staking algorithm or by "mining" with the centralized rewarding system based on Proof-of-Work algorithms, BrainMining (solve math problems and other kind of tasks to gain DIRAs) or CPU Mining
- Message system: send encrypted messages through the blockchain
- Data cloud: upload files (up to 42kb) to the blockchain 
- Voting System: create your own poll

  **Visit <http://www.diracoin.org/> for detailed informations.**


----
## Download the client! ##

  - *pre-packaged* - `http://www.diracoin.org/`

  - *dependencies*:
    - *general* - Java 8
    - *Ubuntu* - `http://www.webupd8.org/2012/09/install-oracle-java-8-in-ubuntu-via-ppa.html`
    - *Debian* - `http://www.webupd8.org/2014/03/how-to-install-oracle-java-8-in-debian.html`
    - *FreeBSD* - `pkg install openjdk8`

  - *repository* - `git clone https://bitbucket.org/diracoin/dira.git`
  
----
## Run on windows! ##

  1) Start DIRA by clicking on the "`START DIRA`" icon at root folder or in alternatives:
- click on "`Start DIRA with console`" (or `run.bat` at the "DIRA" subfolder)					                     
- start from the command line (at the "DIRA" subfolder): `java -jar -Dnxt.runtime.mode=desktop dira.jar`

2) Wait for the JavaFX wallet window to open

  ----
## Run on Unix and Mac! ##

1) Start from the command line (at the "DIRA" sub folder") and run:
 	 
  - Unix: `./start.sh`
  - Mac: `./run.command`  

  2) Wait for the JavaFX wallet window to open
  - on platforms without JavaFX, open <http://localhost:3787/> in a browser

----
## Compile it! ##

  - if necessary with: `./compile.sh` (Unix/Mac) or `./win-compile.sh` (Windows)
  - you need jdk-8 as well

----

## Troubleshooting ##

  - How to Stop the DIRA Server?
    - run `./stop.sh`
    - or if started from command line, ctrl+c or close the console window

  - UI Errors or Stacktraces?
    - report on BitBucket

  - Permissions Denied?
    - no spaces and only latin characters in the path to the NRS installation directory
    - known jetty issue

----
## Further Reading ##

- Visit the official web site at **<http://www.diracoin.org/>** and go to the client section.


