/******************************************************************************
 * Copyright © 2013-2016 The Nxt Core Developers.                             *
 * Copyright © 2016-2017 Jelurida IP B.V.                                     *
 *                                                                            *
 * See the LICENSE.txt file at the top-level directory of this distribution   *
 * for licensing information.                                                 *
 *                                                                            *
 * Unless otherwise agreed in a custom licensing agreement with Jelurida B.V.,*
 * no part of the Nxt software, including this file, may be copied, modified, *
 * propagated, or distributed except according to the terms contained in the  *
 * LICENSE.txt file.                                                          *
 *                                                                            *
 * Removal or modification of this copyright notice is prohibited.            *
 *                                                                            *
 ******************************************************************************/

//TODO: manually add bootstrap nodes
RemoteNodesManager.prototype.REMOTE_NODES_BOOTSTRAP =
{
    "peers": [
				{
					"downloadedVolume": 6378,
					"address": "138.68.165.202",
					"inbound": false,
					"blockchainState": "UP_TO_DATE",
					"weight": 0,
					"uploadedVolume": 7738,
					"services": [
						"API",
						"CORS"
					],
					"requestProcessingTime": 1,
					"version": "1.0.0",
					"platform": "Linux amd64",
					"inboundWebSocket": false,
					"lastUpdated": 134912,
					"blacklisted": false,
					"announcedAddress": "138.68.165.202",
					"apiPort": 3787,
					"application": "DIRA",
					"port": 40874,
					"outboundWebSocket": true,
					"lastConnectAttempt": 134912,
					"state": 1,
					"shareAddress": true
				},
				{
					"downloadedVolume": 9430,
					"address": "159.65.31.71",
					"inbound": false,
					"blockchainState": "UP_TO_DATE",
					"weight": 0,
					"uploadedVolume": 10990,
					"services": [
						"API",
						"CORS"
					],
					"requestProcessingTime": 0,
					"version": "1.0.0",
					"platform": "Linux amd64",
					"inboundWebSocket": false,
					"lastUpdated": 134912,
					"blacklisted": false,
					"announcedAddress": "159.65.31.71",
					"apiPort": 3787,
					"application": "DIRA",
					"port": 40874,
					"outboundWebSocket": true,
					"lastConnectAttempt": 134912,
					"state": 1,
					"shareAddress": true
				}
			]
};
